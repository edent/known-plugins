<?php

    namespace IdnoPlugins\Untappd {

        class ContentType extends \Idno\Common\ContentType {

            public $title = 'Beer Reviews';
            public $category_title = 'Untappd';
            public $entity_class = 'IdnoPlugins\\Untappd\\Untappd';
            public $logo = '<i class="icon-qrcode"></i>';

        }

    }
    
<?php

    namespace IdnoPlugins\Untappd {

        class Untappd extends \Idno\Common\Entity
        {

            function getTitle()
            {
                if (empty($this->title)) return 'Untitled';

                return $this->title;
            }

            function getDescription()
            {
                if (!empty($this->body)) return $this->body;

                return '';
            }

            /**
             * Entry objects have type 'article'
             * @return 'article'
             */
            function getActivityStreamsObjectType()
            {
                return 'article';
            }

            function saveDataFromInput()
            {

                if (empty($this->_id)) {
                    $new = true;
                } else {
                    $new = false;
                }

                if ($new) {
                    if (!\Idno\Core\Idno::site()->triggerEvent("file/upload",[],true)) {
                        return false;
                    }
                }
                $body = \Idno\Core\Idno::site()->currentPage()->getInput('checkin_comment');
               //  file_put_contents("/tmp/untappdmainbody.txt", var_export($body, true));

               //  if (!empty($this->_id)) {
                  //  
                  //  file_put_contents("/tmp/untappd.txt", var_export($this, true));
                  // die();
                    $this->body = $body;

                    $this->canonical_url = "https://untappd.com/user/" . 
                        \Idno\Core\Idno::site()->currentPage()->getInput('user__user_name') . 
                        "/checkin/" . 
                        \Idno\Core\Idno::site()->currentPage()->getInput('checkin_id');

                    $this->title = \Idno\Core\Idno::site()->currentPage()->getInput('checkin_comment');
                    $this->media = \Idno\Core\Idno::site()->currentPage()->getInput('media__items');

                    $this->brewery_name = \Idno\Core\Idno::site()->currentPage()->getInput('brewery__brewery_name');
                    $this->brewery_label= \Idno\Core\Idno::site()->currentPage()->getInput('brewery__brewery_label');
                    
                    $this->beer_name   = \Idno\Core\Idno::site()->currentPage()->getInput('beer__beer_name');
                    $this->beer_label  = \Idno\Core\Idno::site()->currentPage()->getInput('beer__beer_label');
                    $this->beer_abv    = \Idno\Core\Idno::site()->currentPage()->getInput('beer__beer_abv');
                    $this->rating_score= \Idno\Core\Idno::site()->currentPage()->getInput('rating_score');
                    
                    if ($time = \Idno\Core\Idno::site()->currentPage()->getInput('created_at')) {
                        if ($time = strtotime($time)) {
                            $this->created = $time;
                        }
                    }
                  
                    $this->setAccess(\Idno\Core\Idno::site()->currentPage()->getInput('access','PUBLIC'));
                    if ($this->publish($new)) {

                        if ($this->getAccess() == 'PUBLIC') {
                            \Idno\Core\Webmention::pingMentions($this->getURL(), \Idno\Core\Idno::site()->template()->parseURLs($this->getDescription()));
                        }

                        return true;
                    }
               //  } else {
               //      \Idno\Core\Idno::site()->session()->addErrorMessage('You can\'t save an empty drink review.');
               //  }
                
                return false;

            }

            function deleteData()
            {
                if ($this->getAccess() == 'PUBLIC') {
                    \Idno\Core\Webmention::pingMentions($this->getURL(), \Idno\Core\Idno::site()->template()->parseURLs($this->getDescription()));
                }
            }

        }

    }
    
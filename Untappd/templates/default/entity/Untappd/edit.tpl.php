<?=$this->draw('entity/edit/header');?>
<form action="<?=$vars['object']->getURL()?>" method="post" enctype="multipart/form-data">

    <div class="row">

        <div class="col-md-8 col-md-offset-2 edit-pane">

            <p>
                <?php

                    if (empty($vars['object']->_id)) {

                        ?>
                        <h4>
                            <?= \Idno\Core\Idno::site()->language()->_('Write a Beer Review'); ?>
                            </h4>
                                <div id="photo-preview"></div>
                                    <span class="btn btn-primary btn-file">
                                        <i class="fa fa-upload"></i>
                                           <span id="photo-filename"><?= \Idno\Core\Idno::site()->language()->_('Select a photo'); ?></span> 
                                        <input type="file" name="coverimage" id="coverimage" class="form-control" accept="image/*" capture="camera" onchange="coverPreview(this)"/>
                                    </span>
                            </label>
                        </label>
                    <?php
                    }
                ?>
            </p>
            <div class="content-form">
                <label for="canonical_url"><?= \Idno\Core\Idno::site()->language()->_("Untappd URL"); ?></label>
                    <input type="url" name="canonical_url" id="canonical_url" class="form-control" required placeholder="https://" 
                    value="<?=htmlspecialchars($vars['object']->canonical_url)?>" />
            </div>
            
            <div class="content-form">
                <label for="title"><?= \Idno\Core\Idno::site()->language()->_('Title'); ?></label>
                    <input type="text" name="title" id="title" value="<?=htmlspecialchars($vars['object']->title)?>" class="form-control" placeholder="<?= \Idno\Core\Idno::site()->language()->_('Your comment'); ?>" />
            </div>
            
            <div class="content-form">
                <label for="brewery_name"><?= \Idno\Core\Idno::site()->language()->_('Brewery name'); ?></label>
                    <input type="text" name="brewery_name" id="brewery_name" class="form-control" required placeholder=""
                    value="<?=htmlspecialchars($vars['object']->brewery_name)?>" />
            </div>
            <div class="content-form">
                <label for="brewery_label"><?= \Idno\Core\Idno::site()->language()->_("Brewery's Image URL"); ?></label>
                    <input type="url" name="brewery_label" id="brewery_label" class="form-control" required placeholder="https://" 
                    value="<?=htmlspecialchars($vars['object']->brewery_label)?>" />
            </div>

            <div class="content-form">
                <label for="beer_name"><?= \Idno\Core\Idno::site()->language()->_("Beer's name"); ?></label>
                    <input type="text" name="beer_name" id="beer_name" class="form-control" required placeholder=""
                    value="<?=htmlspecialchars($vars['object']->beer_name)?>" />
            </div>
            <div class="content-form">
                <label for="beer_label"><?= \Idno\Core\Idno::site()->language()->_("Beer's Image URL"); ?></label>
                    <input type="url" name="beer_label" id="beer_label" class="form-control" required placeholder="https://" 
                    value="<?=htmlspecialchars($vars['object']->beer_label)?>" />
            </div>
            <div class="content-form">
                <label for="beer_abv"><?= \Idno\Core\Idno::site()->language()->_("Beer ABV"); ?></label>
                    <input type="number" id="rating" name="rating"
                      placeholder="Min: 0, max: 100"
                      min="0" max="100"
                      step=".01" 
                      value="<?=htmlspecialchars($vars['object']->beer_abv)?>"/>
            </div>
            <div class="content-form">
                <label for="rating_score"><?= \Idno\Core\Idno::site()->language()->_("Rating out of 5"); ?></label>
                    <input type="number" id="rating_score" name="rating_score"
                      placeholder="Min: 0, max: 5"
                      min="0" max="5"
                      step=".25"
                      value="<?=htmlspecialchars($vars['object']->rating_score)?>"/>
            </div>
            <div class="content-form">
                <label for="media"><?= \Idno\Core\Idno::site()->language()->_("Media data"); ?></label>
                    <textarea name="media" id="media" class="form-control bodyInput"
                      required
                    ><?=htmlspecialchars($vars['object']->media)?></textarea>
            </div>  
            
            <?=$this->draw('entity/tags/input');?>
            <?php if (empty($vars['object']->_id)) { ?><input type="hidden" name="forward-to" value="<?= \Idno\Core\Idno::site()->config()->getDisplayURL() . 'content/all/'; ?>" /><?php } ?>
            <?php echo $this->drawSyndication('article', $vars['object']->getPosseLinks()); ?>
            <p>
                <?= \Idno\Core\Idno::site()->actions()->signForm('/text/edit') ?>
                <input type="submit" class="btn btn-primary" value="Save" />
                <input type="button" class="btn" value="Cancel" onclick="hideContentCreateForm();" />
                <?= $this->draw('content/extra'); ?>
                <?= $this->draw('content/access'); ?>
            </p>

        </div>

    </div>
</form>
    <script>
        function coverPreview(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#photo-preview').html('<img src="" id="photopreview" style="display:none; width: 400px">');
                    $('#photo-filename').html('<?= \Idno\Core\Idno::site()->language()->_('Choose different image'); ?>');
                    $('#photopreview').attr('src', e.target.result);
                    $('#photopreview').show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
<?=$this->draw('entity/edit/footer');?>
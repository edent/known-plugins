<?php

    namespace IdnoPlugins\Untappd\Pages {

        class Webhook extends \Idno\Common\Page {
           
           function getContent() {
             echo "Ooops! This page only accepts POST requests.";
          }

            function postContent() {

                $new = false;
                if (!empty($this->arguments)) {
                    $object = \IdnoPlugins\Untappd\Untappd::getByID($this->arguments[0]);
                    file_put_contents("/tmp/untappdobject.txt", var_export($object, true));
                }
                if (empty($object)) {
                    $object = new \IdnoPlugins\Untappd\Untappd();
                }

                if ($object->saveDataFromInput()) {
                    $forward = $this->getInput('forward-to', $object->getDisplayURL());
                    $this->forward($forward);
                }

            }

        }

    }

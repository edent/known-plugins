<?php

    namespace IdnoPlugins\Untappd {

        class Main extends \Idno\Common\Plugin {

            function registerPages() {
                \Idno\Core\Idno::site()->addPageHandler('/untappd/edit/?', '\IdnoPlugins\Untappd\Pages\Edit');
                \Idno\Core\Idno::site()->addPageHandler('/untappd/edit/([A-Za-z0-9]+)/?', '\IdnoPlugins\Untappd\Pages\Edit');
                \Idno\Core\Idno::site()->addPageHandler('/untappd/delete/([A-Za-z0-9]+)/?', '\IdnoPlugins\Untappd\Pages\Delete');
                \Idno\Core\Idno::site()->addPageHandler('/untappd/([A-Za-z0-9]+)/.*', '\Idno\Pages\Entity\View');
                \Idno\Core\Idno::site()->addPageHandler('/untappd/webhook/?', '\IdnoPlugins\Untappd\Pages\Webhook');

            }
            
            function registerTranslations() {

                \Idno\Core\Idno::site()->language()->register(
                    new \Idno\Core\GetTextTranslation(
                        'untappd', dirname(__FILE__) . '/languages/'
                    )
                );
            }
        }

    }
    
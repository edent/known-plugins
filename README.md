# Known Plugins

A selection of plugins for https://withknown.com/

## Untappd
Uses [Zapier](https://zapier.com/) to post your recent Untappd checkins to Known.

1. Sign up to [Zapier](https://zapier.com/)
1. Connect Zapier to your [Untappd](https://untappd.com) account
1. The "Trigger" is an Untappd New Check In
1. The "Action" is a Webhook with the following parameters
   1. URL `https://example.com/untappd/webhook`
   1. Payload type: `JSON`
   1. Wrap Request In Array: `No`
   1. Unflatten `No`
   1. Add these two headers
      1. `X-KNOWN-USERNAME` : Your Known username
      1. `X-KNOWN-SIGNATURE`: Your [calculated signature](http://docs.withknown.com/en/latest/developers/plugins/api/)

## Book Reviews
Post book reviews with proper semantic markup.
<?php
	$object = $vars['object'];
?>

<span itemscope itemtype="https://schema.org/Review">
	<meta itemprop="url" content="<?php echo $object->getURL(); ?>">
	<span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Book">
	<?php
		foreach($object->getAttachments() as $attachment) {
	?>
		<img itemprop="image" src="<?=$attachment['url']?>"
		     width="<?=$object->width?>" height="<?=$object->height?>" alt="Book cover"><br>
<?php
	}
?>

		<meta itemprop="url" content="<?php echo htmlspecialchars($object->book_url); ?>">
		<a href="<?php echo htmlspecialchars($object->book_url); ?>"><span itemprop="name"><?php echo htmlspecialchars($object->title); ?></span></a>
		<meta itemprop="isbn" content="<?php echo htmlspecialchars($vars['object']->isbn); ?>"> 
		by <span itemprop="author" itemscope itemtype="http://schema.org/Person">
				<a href="<?php echo htmlspecialchars($object->author_url); ?>">
					<span itemprop="name"><?php echo htmlspecialchars($object->author); ?></span></a>.
			<link itemprop="sameAs" href="<?php echo htmlspecialchars($object->author_url); ?>">
		</span>
	</span>
<br />
<span itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
	<meta itemprop="name" content="<?php echo htmlspecialchars($object->publisher); ?>">
	Published by <?php echo htmlspecialchars($object->publisher); ?>.
</span>
<br />
<meta itemprop="description" content="<?php echo $this->sampleTextChars(htmlspecialchars($object->body ), 199) ; ?>">
<span itemprop="reviewBody"><?php echo $object->body; ?></span><br />
<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
	Rating: <span itemprop="ratingValue"><?php echo htmlspecialchars($object->rating); ?></span>/<span itemprop="bestRating">5</span>
</span><br />
<span itemprop="author" itemscope itemtype="http://schema.org/Person">
	<meta itemprop="name" content="<?php echo $object->getOwner()->getName(); ?>">
	<link itemprop="sameAs" href="<?php echo $object->getOwner()->getURL(); ?>">
</span>
<br />
<meta itemprop="datePublished" content="<?php echo date('Y-m-d\TH:i:s.Z\Z', $object->created); ?>">
<?=$this->draw('entity/edit/header');?>
<form action="<?=$vars['object']->getURL()?>" method="post" enctype="multipart/form-data">

    <div class="row">

        <div class="col-md-8 col-md-offset-2 edit-pane">

            <p>
                <?php

                    if (empty($vars['object']->_id)) {

                        ?>
                        <h4>
                            <?= \Idno\Core\Idno::site()->language()->_('Write a Book Review'); ?>
                            </h4>
                                <div id="photo-preview"></div>
                                    <span class="btn btn-primary btn-file">
                                        <i class="fa fa-upload"></i>
                                           <span id="photo-filename"><?= \Idno\Core\Idno::site()->language()->_('Select a cover image'); ?></span> 
                                        <input type="file" name="coverimage" id="coverimage" class="form-control" required accept="image/*" capture="camera" onchange="coverPreview(this)"/>
                                    </span>
                            </label>
                        </label>
                    <?php
                    }
                ?>
            </p>
            <div class="content-form">
                <label for="title"><?= \Idno\Core\Idno::site()->language()->_('Title'); ?></label>
                    <input type="text" name="title" id="title" value="<?=htmlspecialchars($vars['object']->title)?>" class="form-control" placeholder="<?= \Idno\Core\Idno::site()->language()->_('Book title'); ?>" />
            </div>
            
            <div class="content-form">
                <label for="isbn"><?= \Idno\Core\Idno::site()->language()->_('ISBN'); ?></label>
                    <input type="text" name="isbn" id="isbn" class="form-control" required placeholder=""
                    value="<?=htmlspecialchars($vars['object']->isbn)?>" />
            </div>

            <div class="content-form">
                <label for="book_url"><?= \Idno\Core\Idno::site()->language()->_("Book's URL"); ?></label>
                    <input type="url" name="book_url" id="book_url" class="form-control" required placeholder="https://" 
                    value="<?=htmlspecialchars($vars['object']->book_url)?>" />
            </div>

            <div class="content-form">
                <label for="author"><?= \Idno\Core\Idno::site()->language()->_("Author's name"); ?></label>
                    <input type="text" name="author" id="author" class="form-control" required placeholder=""
                    value="<?=htmlspecialchars($vars['object']->author)?>" />
            </div>
            
            <div class="content-form">
                <label for="author_url"><?= \Idno\Core\Idno::site()->language()->_("Author's URL"); ?></label>
                    <input type="url" name="author_url" id="author_url" class="form-control" required placeholder="https://" 
                    value="<?=htmlspecialchars($vars['object']->author_url)?>" />
            </div>
            
            <div class="content-form">
                <label for="publisher"><?= \Idno\Core\Idno::site()->language()->_("Publisher's name"); ?></label>
                    <input type="text" name="publisher" id="publisher" class="form-control" required placeholder=""
                    value="<?=htmlspecialchars($vars['object']->publisher)?>" />
            </div>

            <div class="content-form">
                <label for="body"><?= \Idno\Core\Idno::site()->language()->_("Write your review"); ?></label>
                    <textarea name="body" id="body" class="form-control bodyInput"
                      required
                    ><?=htmlspecialchars($vars['object']->body)?></textarea>
            </div>  
                    
            <div class="content-form">
                <label for="rating"><?= \Idno\Core\Idno::site()->language()->_("Rating out of 5"); ?></label>
                    <input type="number" id="rating" name="rating"
                      placeholder="Min: 0, max: 5"
                      min="0" max="5" 
                      value="<?=htmlspecialchars($vars['object']->rating)?>"/>
            </div>
            
            <?=$this->draw('entity/tags/input');?>
            <?php if (empty($vars['object']->_id)) { ?><input type="hidden" name="forward-to" value="<?= \Idno\Core\Idno::site()->config()->getDisplayURL() . 'content/all/'; ?>" /><?php } ?>
            <?php echo $this->drawSyndication('article', $vars['object']->getPosseLinks()); ?>
            <p>
                <?= \Idno\Core\Idno::site()->actions()->signForm('/text/edit') ?>
                <input type="submit" class="btn btn-primary" value="Save" />
                <input type="button" class="btn" value="Cancel" onclick="hideContentCreateForm();" />
                <?= $this->draw('content/extra'); ?>
                <?= $this->draw('content/access'); ?>
            </p>

        </div>

    </div>
</form>
    <script>
        function coverPreview(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#photo-preview').html('<img src="" id="photopreview" style="display:none; width: 400px">');
                    $('#photo-filename').html('<?= \Idno\Core\Idno::site()->language()->_('Choose different image'); ?>');
                    $('#photopreview').attr('src', e.target.result);
                    $('#photopreview').show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
<?=$this->draw('entity/edit/footer');?>
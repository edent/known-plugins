<?php

    namespace IdnoPlugins\BookReview {

        class ContentType extends \Idno\Common\ContentType {

            public $title = 'Book Reviews';
            public $category_title = 'Book Reviews';
            public $entity_class = 'IdnoPlugins\\BookReview\\BookReview';
            public $logo = '<i class="icon-qrcode"></i>';

        }

    }
    
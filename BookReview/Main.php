<?php

    namespace IdnoPlugins\BookReview {

        class Main extends \Idno\Common\Plugin {

            function registerPages() {
                \Idno\Core\Idno::site()->addPageHandler('/bookreview/edit/?', '\IdnoPlugins\BookReview\Pages\Edit');
                \Idno\Core\Idno::site()->addPageHandler('/bookreview/edit/([A-Za-z0-9]+)/?', '\IdnoPlugins\BookReview\Pages\Edit');
                \Idno\Core\Idno::site()->addPageHandler('/bookreview/delete/([A-Za-z0-9]+)/?', '\IdnoPlugins\BookReview\Pages\Delete');
                \Idno\Core\Idno::site()->addPageHandler('/bookreview/([A-Za-z0-9]+)/.*', '\Idno\Pages\Entity\View');
            }
            
            function registerTranslations() {

                \Idno\Core\Idno::site()->language()->register(
                    new \Idno\Core\GetTextTranslation(
                        'bookreview', dirname(__FILE__) . '/languages/'
                    )
                );
            }
        }

    }
    